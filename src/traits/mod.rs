pub trait ForwardPass {
    type I;
    type O;
    fn forward(&self, Self::I) -> Self::O;
}

pub trait BackwardPass {
    type I;
    type O;
    fn backward(&self, Self::I) -> Self::O;
}

pub trait Node<T: ForwardPass, BackwardPass> {}
