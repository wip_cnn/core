#[derive(Debug, PartialEq, Copy, Clone)]
pub enum Piece {
    WhiteRook,
    WhiteBishop,
    WhiteKnight,
    WhiteQueen,
    WhiteKing,
    WhitePawn,
    BlackRook,
    BlackBishop,
    BlackKnight,
    BlackQueen,
    BlackKing,
    BlackPawn,
}

impl Piece {
    /// Creates `Piece` from color and piece_type, represented by `&str`.
    /// Color should be formatted as `white|black`
    /// Piece type should be formatted as `rook|bishop|knight|queen|king|pawn`
    /// Currently panics if input is not formatted properly
    ///
    /// # Examples
    ///
    /// Creation of black bishop
    ///
    /// ```
    /// use builder_core::utils::piece::Piece;
    /// let color = "black";
    /// let piece_type = "bishop";
    /// let black_bishop_piece = Piece::new(color, piece_type);
    /// assert_eq!(Piece::BlackBishop, black_bishop_piece);
    /// ```
    pub fn new(color: &str, piece_type: &str) -> Self {
        if color == "white" {
            match piece_type {
                "rook" => Piece::WhiteRook,
                "bishop" => Piece::WhiteBishop,
                "knight" => Piece::WhiteKnight,
                "queen" => Piece::WhiteQueen,
                "king" => Piece::WhiteKing,
                "pawn" => Piece::WhitePawn,
                _ => panic!("Wrong piece type"),
            }
        } else if color == "black" {
            match piece_type {
                "rook" => Piece::BlackRook,
                "bishop" => Piece::BlackBishop,
                "knight" => Piece::BlackKnight,
                "queen" => Piece::BlackQueen,
                "king" => Piece::BlackKing,
                "pawn" => Piece::BlackPawn,
                _ => panic!(format!("Wrong piece type: {}", piece_type)),
            }
        } else {
            panic!("Wrong color");
        }
    }

    /// Parse file name and return result piece type. File name should be formatted as `color_type_*`
    ///
    /// # Examples
    ///
    /// Creating black bishop `Piece` from `"black_bishop_"`
    ///
    /// ```no_run
    /// use builder_core::utils::piece::Piece;
    /// let black_bishop_piece = Piece::from_string("black_bishop_");
    /// assert_eq!(Piece::BlackBishop, black_bishop_piece);
    /// ```
    ///
    pub fn from_string(file_name: &str) -> Self {
        let (color, piece_type) = file_name.split_at(file_name.find('_').unwrap() + 1);
        let color = match color {
            "black_" => "black",
            "white_" => "white",
            _ => panic!("Wrong file name"),
        };
        let piece_type = &piece_type[0..piece_type.find('_').unwrap()];
        Piece::new(color, piece_type)
    }
}
