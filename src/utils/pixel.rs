use image::Primitive;

#[derive(Debug, Copy, Clone)]
pub struct Pixel<P>
where
    P: Primitive,
{
    r: P,
    g: P,
    b: P,
}

impl<P> Pixel<P>
where
    P: Primitive,
{
    pub fn new(rgba: &[P; 3]) -> Self {
        Pixel {
            r: rgba[0],
            g: rgba[1],
            b: rgba[2],
        }
    }

    pub fn rgb_diff(&self, other: &Pixel<P>) -> f64 {
        let self_r = self.r.to_i64().unwrap();
        let self_g = self.g.to_i64().unwrap();
        let self_b = self.b.to_i64().unwrap();
        let other_r = other.r.to_i64().unwrap();
        let other_g = other.g.to_i64().unwrap();
        let other_b = other.b.to_i64().unwrap();
        ((self_r - other_r).abs() + (self_g - other_g).abs() + (self_b - other_b).abs()) as f64
    }
}
