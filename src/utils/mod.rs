pub use rayon::prelude::*;
use std::fs::DirEntry;

pub mod pixel;
pub mod piece;
use utils::pixel::Pixel;
use std::path::Path;

///Returns parallel iterator over all items in directory, represented by `DirEntry`
/// # Examples
///
/// Create a `Vec` of all files in directory
///
/// ```no_run
/// # use builder_core::utils::*;
/// # use std::path::Path;
/// # use std::fs::DirEntry;
///
/// let path = Path::new("path/to/dir");
/// let all_files_in_dir = dir_content(path)
///                         .filter(|entry|entry.path().is_file())
///                         .collect::<Vec<_>>();
/// ```
pub fn dir_content(path: &Path) -> impl ParallelIterator<Item = DirEntry> {
    ::std::fs::read_dir(path)
        .expect(&format!("Failed to open {:?}", path))
        .into_iter()
        .map(|item| item.unwrap())
        .collect::<Vec<_>>()
        .into_par_iter()
}


pub fn l2_distance(first: &[Pixel<u8>], other: &[Pixel<u8>]) -> f64 {
    first
        .iter()
        .zip(other.iter())
        .fold(0.0, |acc, item| acc + item.0.rgb_diff(item.1))
        .sqrt()
}
