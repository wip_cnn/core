use traits::*;
#[allow(dead_code)]
pub struct Node<T: ForwardPass + BackwardPass> {
    next_node: Option<Box<Node<T>>>,
    previous_node: Option<Box<Node<T>>>,
    node: T,
}
fn train() {
    /*
    let mut model = Model::new(ModelType::Sequential);
    model.add(Convolution2d::new(
        48,
        3,
        3,
        Border::Same,
        Some((3, 32, 32)),
    ));
    model.add(Activation::new_relu());
    model.add(Convolution2d::new(48, 3, 3, Border::Same, None));
    */
}
impl<T> Node<T>
where
    T: ForwardPass + BackwardPass,
{
}
