use training_item::TrainingItem;
use utils::pixel::Pixel;
use utils::l2_distance;
use rayon::prelude::*;
pub struct KNearest {
    images: Vec<TrainingItem>,
}

impl KNearest {
    pub fn new(training_data: Vec<TrainingItem>) -> Self {
        KNearest {
            images: training_data,
        }
    }

    pub fn classify_image(&self, image: &DynamicImage) -> u8 {
        let image = match *image {
            DynamicImage::ImageRgb8(ref image) => image,
            _ => unreachable!(),
        };
        let pixels = image.pixels().fold(Vec::new(), |mut acc, pixel| {
            acc.push(Pixel::new(&pixel.data));
            acc
        });
        let mut closest_match = 1;
        let mut min_diff = ::std::f64::MAX;
        for training_image in &self.images {
            let diff = l2_distance(training_image.get_pixels(), &pixels);
            if diff < min_diff {
                closest_match = training_image.get_piece_type();
                min_diff = diff;
            }
        }
        closest_match
    }
}

use cifar_10_loader::*;
use image::DynamicImage;
pub struct KNearestCIFAR {
    pub training_data: Vec<CifarImage>,
    pub test_data: Vec<CifarImage>,
}

impl KNearestCIFAR {
    pub fn new(data_set: CifarDataset) -> KNearestCIFAR {
        KNearestCIFAR {
            training_data: data_set.train_dataset,
            test_data: data_set.test_dataset,
        }
    }

    pub fn convert_to_k_nearest(&self) -> KNearest {
        let mut images = Vec::with_capacity(50000);
        for image in self.training_data.iter().take(50000) {
            let label = image.label;
            let image = match image.image {
                DynamicImage::ImageRgb8(ref image) => image,
                _ => unreachable!(),
            };
            let pixels = image.pixels().fold(Vec::new(), |mut acc, pixel| {
                acc.push(Pixel::new(&pixel.data));
                acc
            });
            let training_item = TrainingItem::new(pixels, label);
            images.push(training_item);
        }
        KNearest::new(images)
    }

    pub fn test_classification(&self, k_nearest: &KNearest, images_limit: usize) -> f64 {
        let mut to_iterate = self.test_data
            .iter()
            .take(images_limit)
            .map(|item| (item, false))
            .collect::<Vec<_>>();
        to_iterate.par_iter_mut().for_each(|image| {
            let label = image.0.label;
            let prediction = k_nearest.classify_image(&image.0.image);
            if prediction == label {
                image.1 = true;
            }
        });

        let correct_results = to_iterate.iter().filter(|item| item.1).count();
        correct_results as f64 / images_limit as f64 * 100.
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;
    use open_dataset;

    /*
    #[bench]
    fn test_10_image_classification_k_nearest(b: &mut Bencher) {
        let cifar_dataset = open_dataset("/home/rizakrko/cifar-10-batches-bin");
        let k_nearest_cifar = KNearestCIFAR::new(cifar_dataset);
        let algo = k_nearest_cifar.convert_to_k_nearest();
        b.iter(|| k_nearest_cifar.test_classification(&algo, 10));
    }
*/
    //Main time is spent in open_dataset function
    //No reason to bench
    /*
    #[bench]
    fn bench_creation(b: &mut Bencher) {
        b.iter(|| {
            let cifar_dataset = open_dataset("/home/rizakrko/cifar-10-batches-bin");
            //let k_nearest_cifar = KNearestCIFAR::new(cifar_dataset);
        });
    }
    */
}
