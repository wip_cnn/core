use utils::pixel::Pixel;

/// Type represents each image in data set. Contains `Vec<Pixel<u8>>` as pixels rgba values and `Piece` as type
#[derive(Debug)]
pub struct TrainingItem {
    piece_type: u8,
    pixels: Vec<Pixel<u8>>,
}

impl TrainingItem {
    /// Builds `TrainingItem` from file at path. Return `Err(ImageError)` if couldn't open image from path
    /*pub fn from_path(path: &impl AsRef<Path>) -> Result<Self, ImageError> {
        let image = open(path)?;
        let pixels = image.pixels().fold(Vec::new(), |mut acc, pixel| {
            acc.push(Pixel::new(&pixel.2.data));
            acc
        });
        let temp_piece_type =
            Piece::from_string(path.as_ref().file_name().unwrap().to_str().unwrap());
        Ok(TrainingItem {
            piece_type: temp_piece_type,
            pixels,
        })
    }*/

    pub fn new(pixels: Vec<Pixel<u8>>, piece_type: u8) -> Self {
        TrainingItem { pixels, piece_type }
    }

    pub fn get_piece_type(&self) -> u8 {
        self.piece_type
    }
    pub fn get_pixels(&self) -> &Vec<Pixel<u8>> {
        &self.pixels
    }
}
