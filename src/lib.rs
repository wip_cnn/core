#![feature(universal_impl_trait)]
#![feature(conservative_impl_trait)]
#![feature(nll)]
#![feature(test)]
#![feature(const_fn)]
#![feature(fn_traits)]

extern crate cifar_10_loader;
extern crate image;
extern crate nalgebra;
extern crate rand;
extern crate rayon;
extern crate test;
pub use cifar_10_loader::CifarDataset;

pub mod utils;
pub mod training_item;
pub mod algorithms;
pub mod loss;
pub mod traits;
pub mod node;
pub mod model;
pub mod layer;
pub fn open_dataset(path: &str) -> CifarDataset {
    CifarDataset::new(path).unwrap()
}
