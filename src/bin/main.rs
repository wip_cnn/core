#![allow(dead_code)]
extern crate builder_core;

extern crate dotenv;
#[macro_use]
extern crate dotenv_codegen;
extern crate nalgebra;

static TEST_IMAGES: &'static str = dotenv!("TEST_IMAGES");
static LABELED_IMAGES_PATH: &'static str = dotenv!("LABELED_IMAGES_PATH");
static CIFAR_10_IMAGES: &'static str = dotenv!("CIFAR_10_IMAGES");

use builder_core::model::{Model, ModelType};
use builder_core::layer::convolution2d::Convolution2d;
use builder_core::layer::Border;
use builder_core::layer::activation::Activation;
use builder_core::layer::pooling::MaxPooling2d;
use builder_core::layer::MultidimensionalLayer;
use builder_core::layer::stretch_layer::StretchLayer;
use builder_core::layer::linear_layer::LinearLayer;

use nalgebra::DMatrix;

fn main() {
    let mut model = Model::new(ModelType::Sequential);
    model.add_layer(Box::new(Convolution2d::new(48, 3, 3, 3, Border::new(0, 0))));
    model.add_layer(Box::new(Activation::re_lu()));
    model.add_layer(Box::new(MaxPooling2d::new(2, 2)));
    model.add_layer(Box::new(StretchLayer::new()));
    model.add_layer(Box::new(LinearLayer::new(192, 10)));
    let test_input = vec![
        DMatrix::from_column_slice(
            7,
            7,
            &[
                0., 1., 1., 1., 0., 0., 0., 0., 0., 1., 1., 1., 0., 0., 0., 0., 0., 1., 1., 1., 0.,
                0., 0., 0., 1., 1., 0., 0., 0., 0., 1., 1., 0., 0., 0., 0., 1., 1., 0., 0., 0., 0.,
                1., 1., 0., 0., 0., 0., 0.,
            ],
        ),
    ];
    let test_matrix = MultidimensionalLayer::new(test_input);
    model.train(&test_matrix);
    //   model.add(Convolution2d::new(48, 3, 3, Border::Same, None));
}

/*
fn main() {
    dotenv::dotenv().ok();
    let all_files: Vec<DirEntry> = dir_content(Path::new(LABELED_IMAGES_PATH))
        .filter(|item| item.path().is_file())
        .collect();
    let training_items = all_files
        .iter()
        .map(|item| TrainingItem::from_path(&item.path()).unwrap())
        .collect::<Vec<TrainingItem>>();
    let algo = KNearest::new(training_items);
    let all_files: Vec<DirEntry> = dir_content(Path::new(TEST_IMAGES))
        .filter(|item| item.path().is_file())
        .collect();
    let training_items = all_files
        .iter()
        .map(|item| TrainingItem::from_path(&item.path()).unwrap())
        .collect::<Vec<TrainingItem>>();
    let test_images = training_items
        .iter()
        .map(|item| (item.get_pixels().clone(), item.get_piece_type()))
        .collect::<Vec<_>>();
    let amount_og_test_images = test_images.len();
    let mut successful_classifications = 0;
    for test_image in &test_images {
        let classification_result = algo.classify_item(test_image.0.to_vec());
        if classification_result == *test_image.1 {
            successful_classifications += 1;
        }
        eprintln!(
            "Approximation result: {:?}, true type: {:?}",
            algo.classify_item(test_image.0.to_vec()),
            test_image.1
        );
    }
    let percentage = successful_classifications as f64 / amount_og_test_images as f64 * 100.;
    eprintln!("Correct: {}%\nWrong: {}%", percentage, 100.0 - percentage);
}
*/
//fn main() {
/*let cifar_dataset = k_nearest::open_dataset(CIFAR_10_IMAGES);
let k_nearest_cifar = k_nearest::algorithms::KNearestCIFAR::new(cifar_dataset);
let algo = k_nearest_cifar.convert_to_k_nearest();
let succ_rate = k_nearest_cifar.test_classification(&algo, 1000);
eprintln!("Succ rate: {}", succ_rate);
*/
//    k_nearest::loss::tt();

//}
