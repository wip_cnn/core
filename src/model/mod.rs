use super::layer::Layer;
use super::nalgebra::DMatrix;
use layer::MultidimensionalLayer;

pub struct Model {
    model_type: ModelType,
    layers: Vec<Box<Layer>>,
}

impl Model {
    pub fn new(model_type: ModelType) -> Self {
        Model {
            model_type,
            layers: Vec::new(),
        }
    }

    pub fn add_layer(&mut self, layer: Box<Layer>) {
        self.layers.push(layer);
    }

    pub fn train(&mut self, input: &MultidimensionalLayer) {
        loop {
            let fp_result = self.forward_pass(input);
            self.backprop(&fp_result);
            return;
        }
    }

    pub fn forward_pass(&mut self, input: &MultidimensionalLayer) -> MultidimensionalLayer {
        let mut layer_result = (*input).clone();
        eprintln!("Input: {:#?}", layer_result);
        for layer in &self.layers {
            layer_result = layer.forward_pass(&layer_result);
            eprintln!("After layer: {:#?}", layer_result);
        }
        layer_result
    }
    pub fn backprop(&mut self, fp_result: &MultidimensionalLayer) {}
}

pub enum ModelType {
    Sequential,
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_model_creation() {
        let _model = Model::new(ModelType::Sequential);
    }
}
