struct Loss<L>
where
    L: LossFunction,
{
    loss_function: L,
}

trait LossTrait<L>
where
    L: LossFunction,
{
    fn process(&self, input: L::I) -> L::O;
}

impl<L> LossTrait<L> for Loss<L>
where
    L: LossFunction,
{
    fn process(&self, input: L::I) -> L::O {
        self.loss_function.process(input)
    }
}

trait LossFunction {
    type I;
    type O;
    fn process(&self, input: Self::I) -> Self::O;
}

fn multiclass_svn<'a>((values, current_index): (&'a [f64], usize)) -> f64 {
    let current_value = values[current_index];
    values
        .iter()
        .map(move |item| {
            if 0. >= item - current_value + 1. {
                0.
            } else {
                item - current_value + 1.
            }
        })
        .sum::<f64>() - 1.
}
struct MulticlassSVN<'a> {
    loss_f: fn((&'a [f64], usize)) -> f64,
}

impl<'a> MulticlassSVN<'a> {
    fn new() -> Self {
        MulticlassSVN {
            loss_f: multiclass_svn,
        }
    }
}

impl<'a> LossFunction for MulticlassSVN<'a> {
    type I = (&'a [f64], usize);
    type O = f64;

    fn process(&self, input: Self::I) -> Self::O {
        self.loss_f.call((input,))
    }
}

impl<'a> Loss<MulticlassSVN<'a>> {
    fn new() -> Self {
        Loss {
            loss_function: MulticlassSVN::new(),
        }
    }
}

pub fn tt() {
    let loss = Loss::<MulticlassSVN>::new();
    let vec = vec![3.2, 5.1, -1.7];
    let x = loss.process((&vec, 0));
    eprintln!("result: {}", x);
}
