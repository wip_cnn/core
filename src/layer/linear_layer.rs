use super::rand;
use rand::distributions::range::Range;
use rand::distributions::IndependentSample;
use layer::Layer;
use layer::MultidimensionalLayer;
use super::DMatrix;

pub struct LinearLayer {
    from: usize,
    to: usize,
    weights: Vec<Vec<f64>>,
}

impl Layer for LinearLayer {
    fn forward_pass(&self, input: &MultidimensionalLayer) -> MultidimensionalLayer {
        eprintln!("{} {}", self.weights.len(), self.weights[0].len());
        eprintln!("{}", input.get_data()[0].len());
        assert_eq!(input.get_data().len(), 1);
        let mut result = Vec::new();
        let input_data = input.get_data();
        for i in 0..self.weights.len() {
            let current_input = &input_data[0];
            let mut inner_result = Vec::new();
            for j in 0..current_input.len() {
                inner_result.push(current_input[j] * self.weights[i][j]);
            }
            result.push(inner_result);
        }
        let stretched_result = stretch_vec_vec(result);
        let matrix = DMatrix::from_column_slice(1, self.from * self.to, &stretched_result);
        MultidimensionalLayer::new(vec![matrix])
    }
}

fn stretch_vec_vec(input: Vec<Vec<f64>>) -> Vec<f64> {
    let mut result = Vec::new();
    for vec in input {
        result.extend(vec.iter());
    }
    result
}

impl LinearLayer {
    pub fn new(from: usize, to: usize) -> Self {
        let mut weights = Vec::new();
        let mut rng = rand::thread_rng();
        let between = Range::new(0f64, 0.001);
        for _ in 0..to {
            let mut inner_vec = Vec::new();
            for _ in 0..from {
                inner_vec.push(between.ind_sample(&mut rng));
            }
            weights.push(inner_vec);
        }
        LinearLayer { from, to, weights }
    }
}
