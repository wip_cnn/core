use super::nalgebra::*;
use super::rand;
use super::rand::distributions::{IndependentSample, Range};
pub mod convolution2d;
pub mod pooling;
pub mod activation;
pub mod stretch_layer;
pub mod linear_layer;
pub struct Filter {
    filter_layers: MultidimensionalLayer,
}

#[derive(Clone, Debug)]
pub struct MultidimensionalLayer {
    data: Vec<DMatrix<f64>>,
}

impl MultidimensionalLayer {
    pub fn new(data: Vec<DMatrix<f64>>) -> Self {
        MultidimensionalLayer { data }
    }

    pub fn get_dimensions(&self) -> (usize, usize, usize) {
        assert!(!self.data.is_empty());
        let depth = self.data.len();
        let layer = &self.data[0];
        (layer.ncols(), layer.nrows(), depth)
    }

    pub fn get_data_mut(&mut self) -> &mut Vec<DMatrix<f64>> {
        &mut self.data
    }

    pub fn get_data(&self) -> &Vec<DMatrix<f64>> {
        &self.data
    }
}

impl Filter {
    pub fn new(width: u64, height: u64, dimension: u64) -> Self {
        let mut result_vector = Vec::new();
        for _ in 0..dimension {
            let mut rng = rand::thread_rng();
            let between = Range::new(0f64, 0.001);
            let mut initial_values = Vec::with_capacity((width * height) as usize);
            for _ in 0..width * height {
                initial_values.push(between.ind_sample(&mut rng));
            }
            let matrix =
                DMatrix::from_column_slice(width as usize, height as usize, &initial_values);
            result_vector.push(matrix);
        }
        Filter {
            filter_layers: MultidimensionalLayer::new(result_vector),
        }
    }

    pub fn get_filter_layers(&self) -> &MultidimensionalLayer {
        &self.filter_layers
    }

    fn new_preset(filter_layers: MultidimensionalLayer) -> Self {
        Filter { filter_layers }
    }
}

pub trait Layer {
    fn forward_pass(&self, input: &MultidimensionalLayer) -> MultidimensionalLayer;
}

pub struct Border {
    x_offset: i64,
    y_offset: i64,
}

impl Border {
    pub fn new(x_offset: i64, y_offset: i64) -> Self {
        Border { x_offset, y_offset }
    }
}
