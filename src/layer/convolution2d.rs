use layer::Border;
use layer::Filter;
use layer::Layer;
use super::DMatrix;
use super::MultidimensionalLayer;

pub struct Convolution2d {
    filters: Vec<Filter>,
}

impl Layer for Convolution2d {
    fn forward_pass(&self, input: &MultidimensionalLayer) -> MultidimensionalLayer {
        let input = input.get_data();
        assert!(!input.is_empty());
        let (filter_width, filter_height, _dense) = self.get_filter_dimensions();
        let (input_matrix_width, input_matrix_height) = get_dimensions_of_matrix(&input[0]);
        let result_map_width = input_matrix_width - filter_width + 1;
        let result_map_height = input_matrix_height - filter_height + 1;
        let mut result_matrix = Vec::with_capacity(result_map_width * result_map_height);
        for filter in &self.filters {
            let mut activation_map = Vec::new();
            let stretched_filter = stretch_vec_of_matrix(filter.get_filter_layers().get_data());
            for i in 0..result_map_width {
                for j in 0..result_map_height {
                    let n_dim_slice = input
                        .iter()
                        .map(|layer| {
                            layer
                                .slice((j, i), (filter_width, filter_height))
                                .clone_owned()
                        })
                        .collect::<Vec<_>>();
                    let stretched_slice = stretch_vec_of_matrix(&n_dim_slice);
                    activation_map.push(stretched_slice.dot(&stretched_filter));
                }
            }
            let activation_map =
                DMatrix::from_column_slice(result_map_width, result_map_height, &activation_map);
            result_matrix.push(activation_map);
        }
        MultidimensionalLayer::new(result_matrix)
    }
}

impl Convolution2d {
    //TODO Border is currently unused
    pub fn new(
        filters_count: u64,
        filter_dimension: u64,
        filter_width: u64,
        filter_height: u64,
        border: Border,
    ) -> Self {
        let mut filters = Vec::with_capacity(filters_count as usize);
        for _ in 0..filters_count {
            filters.push(Filter::new(filter_width, filter_height, filter_dimension));
        }
        Convolution2d { filters }
    }

    fn get_filter_dimensions(&self) -> (usize, usize, usize) {
        let filter_layer = self.filters[0].get_filter_layers().get_data()[0].clone();
        (
            filter_layer.ncols(),
            filter_layer.nrows(),
            self.filters.len(),
        )
    }

    #[cfg(test)]
    fn new_with_preset_filter(filter: Filter) -> Self {
        Convolution2d {
            filters: vec![filter],
        }
    }
}

fn stretch_vec_of_matrix(chunk: &[DMatrix<f64>]) -> DMatrix<f64> {
    assert!(!chunk.is_empty());
    let dense = chunk.len();
    let (width, height) = get_dimensions_of_matrix(&chunk[0]);
    let mut stretched_values = vec![0.; width * height * dense];
    for (offset, matrix) in chunk.iter().enumerate() {
        for (base_position, point_value) in matrix.iter().enumerate() {
            stretched_values[base_position + offset * dense] = *point_value;
        }
    }

    DMatrix::from_column_slice(1, width * height * dense, &stretched_values)
}

fn get_dimensions_of_matrix(matrix: &DMatrix<f64>) -> (usize, usize) {
    (matrix.ncols(), matrix.nrows())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_stretch() {
        let first_layer = DMatrix::from_column_slice(1, 3, &vec![1., 2., 3.]);
        let second_layer = DMatrix::from_column_slice(1, 3, &vec![4., 5., 6.]);
        let third_layer = DMatrix::from_column_slice(1, 3, &vec![7., 8., 9.]);
        let chunk = vec![first_layer, second_layer, third_layer];
        let stretched = stretch_vec_of_matrix(&chunk);
        assert_eq!(
            stretched,
            DMatrix::from_column_slice(1, 9, &vec![1., 2., 3., 4., 5., 6., 7., 8., 9.])
        );
    }

    #[test]
    fn test_cross_coleration() {
        let filter = Filter::new_preset(MultidimensionalLayer::new(vec![
            DMatrix::from_column_slice(3, 3, &[1., 0., 1., 0., 1., 0., 1., 0., 1.]),
        ]));
        let conv_layer = Convolution2d::new_with_preset_filter(filter);
        let test_input = vec![
            DMatrix::from_column_slice(
                7,
                7,
                &[
                    0., 1., 1., 1., 0., 0., 0., 0., 0., 1., 1., 1., 0., 0., 0., 0., 0., 1., 1., 1.,
                    0., 0., 0., 0., 1., 1., 0., 0., 0., 0., 1., 1., 0., 0., 0., 0., 1., 1., 0., 0.,
                    0., 0., 1., 1., 0., 0., 0., 0., 0.,
                ],
            ),
        ];
        let test_matrix = MultidimensionalLayer::new(test_input);
        let expected_matrix = DMatrix::from_column_slice(
            5,
            5,
            &[
                1.0, 4.0, 3.0, 4.0, 1.0, 1.0, 2.0, 4.0, 3.0, 3.0, 1.0, 2.0, 3.0, 4.0, 1.0, 1.0,
                3.0, 3.0, 1.0, 1.0, 3.0, 3.0, 1.0, 1.0, 0.0,
            ],
        );
        let result = conv_layer.forward_pass(&test_matrix);
        assert_eq!(result.get_data()[0], expected_matrix);
    }
}
