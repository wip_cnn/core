use layer::Layer;
use layer::MultidimensionalLayer;
use super::rand::Rng;
use super::DMatrix;
pub struct StretchLayer {}

impl StretchLayer {
    pub fn new() -> Self {
        StretchLayer {}
    }
}

impl Layer for StretchLayer {
    fn forward_pass(&self, input: &MultidimensionalLayer) -> MultidimensionalLayer {
        assert!(!input.get_data().is_empty());
        let (width, height, depth) = input.get_dimensions();
        let mut result_vec = Vec::new();
        for layer in input.get_data() {
            for i in 0..layer.nrows() {
                for j in 0..layer.ncols() {
                    result_vec.push(layer[(i, j)]);
                }
            }
        }
        let matrix = DMatrix::from_column_slice(1, width * height * depth, &result_vec);
        MultidimensionalLayer::new(vec![matrix])
    }
}
