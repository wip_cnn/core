use super::DMatrix;
use super::MultidimensionalLayer;
use layer::Layer;

pub struct MaxPooling2d {
    width: u64,
    height: u64,
}

impl MaxPooling2d {
    pub fn new(width: u64, height: u64) -> Self {
        MaxPooling2d { width, height }
    }
}

impl Layer for MaxPooling2d {
    fn forward_pass(&self, input: &MultidimensionalLayer) -> MultidimensionalLayer {
        let (width, height, _) = input.get_dimensions();
        let mut pooled_layers = Vec::new();
        for layer in input.get_data() {
            let mut current_layer = Vec::new();
            for j in 0..width / self.width as usize {
                for i in 0..height / self.height as usize {
                    current_layer.push(
                        layer
                            .slice(
                                (i * self.width as usize, j * self.height as usize),
                                (self.width as usize, self.height as usize),
                            )
                            .iter()
                            .fold(::std::f64::NEG_INFINITY, |a, &b| a.max(b)),
                    );
                }
            }
            eprintln!("Matrix: {:?}", current_layer);
            pooled_layers.push(DMatrix::from_column_slice(
                width / self.width as usize,
                height / self.height as usize,
                &current_layer,
            ));
        }
        MultidimensionalLayer::new(pooled_layers)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pooling_with_exact_fit() {
        let mut layer = DMatrix::from_column_slice(
            4,
            4,
            &vec![
                1., 0., 2., 3., 4., 6., 6., 8., 3., 1., 1., 0., 1., 2., 2., 4.
            ],
        );
        let multidim = MultidimensionalLayer::new(vec![layer]);
        let layer = MaxPooling2d::new(2, 2);
        let result = layer.forward_pass(&multidim);
        assert_eq!(result.get_data().len(), 1);
        let expected_result = DMatrix::from_column_slice(2, 2, &vec![6., 8., 3., 4.]);
        assert_eq!(expected_result, result.get_data()[0]);
    }
}
