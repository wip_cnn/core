use super::Layer;
use super::DMatrix;
use super::MultidimensionalLayer;
pub struct Activation {
    activation_function: fn(&MultidimensionalLayer) -> MultidimensionalLayer,
}

impl Activation {
    pub fn re_lu() -> Activation {
        let activation_function = re_lu;
        Activation {
            activation_function,
        }
    }
}

fn re_lu(input: &MultidimensionalLayer) -> MultidimensionalLayer {
    let (width, height, _) = input.get_dimensions();
    let mut input_layers = input.get_data().to_owned();
    for layer in input_layers.iter_mut() {
        for j in 0..width {
            for i in 0..height {
                if layer[(i, j)] <= 0. {
                    layer[(i, j)] = 0.
                }
            }
        }
    }
    MultidimensionalLayer::new(input_layers)
}

impl Layer for Activation {
    fn forward_pass(&self, input: &MultidimensionalLayer) -> MultidimensionalLayer {
        (self.activation_function)(input)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_re_lu_dimension_matching() {
        let first_layer = DMatrix::from_column_slice(1, 3, &vec![1., 0., -1.]);
        let multidimensional_layer = MultidimensionalLayer::new(vec![first_layer]);
        let activation_result = re_lu(&multidimensional_layer);
        assert_eq!(
            multidimensional_layer.get_dimensions(),
            activation_result.get_dimensions()
        );
    }

    #[test]
    fn test_re_lu() {
        let first_layer = DMatrix::from_column_slice(1, 3, &vec![1., 0., -1.]);
        let multidimensional_layer = MultidimensionalLayer::new(vec![first_layer]);
        let activation_result = re_lu(&multidimensional_layer);
        assert_eq!(
            activation_result.get_data(),
            &vec![DMatrix::from_column_slice(1, 3, &vec![1., 0., 0.])]
        );
    }
}
